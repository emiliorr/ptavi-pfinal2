#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socket
import socketserver
import datetime
import threading


IP = "127.0.0.1"
PORT = 34643

# python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio>


def get_time():
    actual_time = datetime.datetime.now()
    formatted_time = actual_time.strftime('%Y%m%d%H%M%S')
    return formatted_time


def get_arg():
    if len(sys.argv) != 3:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>')
    try:
        sipserverip = sys.argv[1].split(':')[0]
        sipserverport = int(sys.argv[1].split(':')[1])
        service = sys.argv[2]
    except ValueError:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>')

    return sipserverip, sipserverport, service


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    output = None

    def handle(self):
        msg = self.request[0]

        payload = msg[12:]

        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def main():
    sipserverip, sipserverport, service = get_arg()

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{get_time()} Starting...')

            register_sip(my_socket, sipserverip, sipserverport, service)

            data, address = my_socket.recvfrom(1024)
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

            if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':
                data2, address = my_socket.recvfrom(1024)
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data2.decode("utf-8")}')

                if data2.decode('utf-8').split()[0] == 'INVITE':
                    client_ip = data2.decode('utf-8').split('o=')[1].split()[1]
                    client_address = data2.decode('utf-8').split('sip:')[2].split('>')[0]
                    client_session = data2.decode('utf-8').split('s=')[1].split('\n')[0]

                    send_response(my_socket, address, service, client_address, client_ip, client_session)

                    print(f'{get_time()} RTP ready {PORT}')

                    receive_rtp()

                    data3, address = my_socket.recvfrom(1024)
                    print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data3.decode("utf-8")}')

                    if data3.decode('utf-8').split()[0] == 'BYE':
                        print(f'{get_time()} RTP all content received {PORT}')
                        send_assent(my_socket, address, service)

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")


def register_sip(socket, ip, port, service):
    register = f'REGISTER sip:{service}@songs.net SIP/2.0\r\n\r\n'
    socket.sendto(register.encode('utf-8'), (ip, port))
    print(f'{get_time()} SIP to {ip}:{port}: {register}')


def send_response(socket, address, service, client_address, client_ip, client_session):
    response = f'SIP/2.0 200 OK'
    head = f'From: <sip:{service}@songs.net>'
    content_type = f'Content-Type: application/sdp'
    body = f'v=0\no=sip:{client_address} {client_ip}\ns={client_session}\nt=0\nm=audio {PORT} RTP'
    content_length = f'Content-Length: {len(body.encode("utf-8"))}'
    message = f'{response}\r\n{head}\r\n{content_type}\r\n{content_length}\r\n\r\n{body}'
    socket.sendto(message.encode('utf-8'), (address[0], address[1]))
    print(f'{get_time()} SIP to {address[0]}:{address[1]}: {message}')


def receive_rtp():
    RTPHandler.open_output('recibido.mp3')
    with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
        print("Listening...")
        threading.Thread(target=serv.serve_forever).start()

        actual_time = datetime.datetime.now()
        target_time = actual_time + datetime.timedelta(seconds=30)
        while datetime.datetime.now() < target_time:
            pass

        print("Time passed, shutting down receiver loop.")

    serv.shutdown()
    RTPHandler.close_output()


def send_assent(socket, address, service):
    assent = f'SIP/2.0 200 OK'
    head = f'From: <sip:{service}@songs.net>'
    message = f'{assent}\r\n{head}\r\n\r\n'
    socket.sendto(message.encode('utf-8'), (address[0], address[1]))
    print(f'{get_time()} SIP to {address[0]}:{address[1]}: {message}')


if __name__ == "__main__":
    main()

