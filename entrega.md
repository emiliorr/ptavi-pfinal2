## Parte básica
- Hay varios errores de PEP8 al pasar el check.py, los cuales no entiendo ya que según PyCharm está todo correcto.
- Según he entendido, el serverRTP deberia quedarse escuchando una vez termina el cliente, y deberia poder recibir otra conversación, pero no lo he conseguido asi que lo que he hecho es apagarlo en cuanto termina el cliente y lo lanzo de nuevo para la segunda parte de la captura.
- En el check estan mal los nombres segun los pedís en el guión, asi que lo he corregido con los correctos y que no salte el error de que faltan ficheros. La carpeta de los tests no ha sido proporcionada (o al menos yo no la encuentro) por lo que da error con eso.

## Parte adicional
- He añadido la cabecera de tamaño simplemente haciendo un len() del body 

## Otra:
- Como creo que ha tenido que hacer el resto de los compañeros, he cambiado el fichero mp3 por el de la práctica de enero porque si no no funcionaba.
- He trabajado completamente en local, por lo que todos los commits han sido a la hora de subir el proyecto al completo, al no haber hecho uso de git.
