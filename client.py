#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import socket
import datetime
import time
import simplertp


IP = '127.0.0.1'
PORT = 45954

# python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>


def get_time():
    actual_time = datetime.datetime.now()
    formatted_time = actual_time.strftime('%Y%m%d%H%M%S')
    return formatted_time


def get_arg():
    if len(sys.argv) != 5:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>')
    try:
        sipserverip = sys.argv[1].split(':')[0]
        sipserverport = int(sys.argv[1].split(':')[1])
        clientaddr = sys.argv[2]
        clientaddrname = sys.argv[2].split(':')[1].split('@')[0]
        rtpserveraddr = sys.argv[3]
        rtpserveraddrname = sys.argv[3].split(':')[1].split('@')[0]
        file = sys.argv[4]
    except ValueError:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>')

    return sipserverip, sipserverport, clientaddr, clientaddrname, rtpserveraddr, rtpserveraddrname, file


def register_sip(sipserverip, sipserverport, clientaddr):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        register = f'REGISTER {clientaddr} SIP/2.0\r\n\r\n'
        my_socket.sendto(register.encode('utf-8'), (sipserverip, sipserverport))
        print(f'{get_time()} SIP to {sipserverip}:{sipserverport}: {register}')

        data, address = my_socket.recvfrom(1024)
        print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

        return data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK'


def send_invite(sipserverip, sipserverport, rtpserveraddr, clientaddr, clientaddrname):
    invite = f'INVITE {rtpserveraddr} SIP/2.0'
    head = f'From: <{clientaddr}>'
    content_type = f'Content-Type: application/sdp'
    body = f'v=0\no={clientaddr} {IP}\ns={clientaddrname}\nt=0\nm=audio {PORT} RTP'
    content_length = f'Content-Length: {len(body.encode("utf-8"))}'
    message = f'{invite}\r\n{head}\r\n{content_type}\r\n{content_length}\r\n\r\n{body}'

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.sendto(message.encode('utf-8'), (sipserverip, sipserverport))
        print(f'{get_time()} SIP to {sipserverip}:{sipserverport}: {message}')

        data, address = my_socket.recvfrom(1014)
        print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

        if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':
            assent_sip = f'ACK {rtpserveraddr} SIP/2.0'
            head = f'From: <{clientaddr}>'
            message_sip = f'{assent_sip}\r\n{head}\r\n\r\n'
            my_socket.sendto(message_sip.encode('utf-8'), (sipserverip, sipserverport))
            print(f'{get_time()} SIP to {sipserverip}:{sipserverport}: {message_sip}')

            rtpserverip = data.decode('utf-8').split('\r\n')[1].split()[1].split('@')[1].split(':')[0]
            rtpserverport_noint = data.decode('utf-8').split('\r\n')[1].split()[1].split('@')[1].split(':')[1]
            rtpserverport = int(rtpserverport_noint)

            invite_rtp = f'INVITE sip:{clientaddrname}@{rtpserverip}:{rtpserverport} SIP/2.0'
            content_type = f'Content-Type: application/sdp'
            body = f'v=0\no={clientaddr} {IP}\ns={clientaddrname}\nt=0\nm=audio {PORT} RTP'
            content_length = f'Content-Length: {len(body.encode("utf-8"))}'
            message_rtp = f'{invite_rtp}\r\n{head}\r\n{content_type}\r\n{content_length}\r\n\r\n{body}'
            my_socket.sendto(message_rtp.encode('utf-8'), (rtpserverip, rtpserverport))
            print(f'{get_time()} SIP to {rtpserverip}:{rtpserverport}: {message_rtp}')

            data, address = my_socket.recvfrom(1024)
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

            if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':
                assent = f'ACK sip:{clientaddrname}@{rtpserverip}:{rtpserverport} SIP/2.0'
                head = f'From: <{clientaddr}>'
                message_ack = f'{assent}\n{head}\r\n\r\n'

                my_socket.sendto(message_ack.encode('utf-8'), (sipserverip, sipserverport))
                print(f'{get_time()} SIP to {sipserverip}:{sipserverport}: {message_ack}')

                received = data.decode('utf-8')
                rtpserver_ip = received.split('o=')[1].split()[1]
                rtpserver_port = int(received.split('audio ')[1].split()[0])

                print(f'{get_time()} RTP to {rtpserver_ip}:{rtpserver_port}')
                sender = simplertp.RTPSender(ip=rtpserver_ip, port=rtpserver_port, file='cancion.mp3', printout=True)
                sender.send_threaded()
                time.sleep(5)
                print("Finalizando el envío.")
                sender.finish()

                bye = f'BYE sip:{clientaddrname}@{rtpserverip}:{rtpserverport} SIP/2.0\r\n\r\n'
                my_socket.sendto(bye.encode('utf-8'), (rtpserverip, rtpserverport))
                print(f'{get_time()} SIP to {rtpserverip}:{rtpserverport}: {bye}')

                data, address = my_socket.recvfrom(1024)
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

                return data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK'


def main():
    result = get_arg()
    sipserverip, sipserverport, clientaddr, clientaddrname, rtpserveraddr, rtpserveraddrname, file = result

    try:
        if register_sip(sipserverip, sipserverport, clientaddr):
            if send_invite(sipserverip, sipserverport, rtpserveraddr, clientaddr, "clientaddrname"):
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.close()

    except ConnectionRefusedError:
        print("Error conexión con servidor")


if __name__ == "__main__":
    main()

