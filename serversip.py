#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socketserver
import json
import datetime


def get_time():
    actual_time = datetime.datetime.now()
    formatted_time = actual_time.strftime('%Y%m%d%H%M%S')
    return formatted_time


def get_arg():
    if len(sys.argv) != 2:
        sys.exit('Usage: python3 serversip.py <port>')
    try:
        port = int(sys.argv[1])
    except ValueError:
        sys.exit('Usage: python3 serversip.py <port>')
    return port


def ex_dicc():
    user_dicc = {}
    try:
        with open('registrar.json', 'r') as fich:
            data = json.load(fich)
            for user in data:
                user_dicc.update(user)
    except FileNotFoundError:
        pass

    return user_dicc


class EchoHandler(socketserver.BaseRequestHandler):

    user_dicc = ex_dicc()

    def pregister(self, sip, real):

        self.user_dicc[sip] = real

        fich = open('registrar.json', 'w')
        fich.write(str(json.dumps([self.user_dicc], indent=1)))
        fich.close()

    def search_address(self, sip_address):

        if sip_address in self.user_dicc:
            return self.user_dicc[sip_address]
        else:
            return 'User Not Found'

    def handle(self):
        data = self.request[0]
        print(f'{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}: {data.decode("utf-8")}')
        sock = self.request[1]
        received = data.decode('utf-8')
        petition = received.split()[0]

        if type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)

        if petition == 'INVITE':
            self.handle_invite(sock, received, self.client_address)

        elif petition == 'REGISTER':
            self.handle_register(sock, received, self.client_address)

        elif petition == 'ACK':
            pass

    def handle_invite(self, sock, received, client_address):
        try:
            user = self.search_address(received.split('From: ')[1].split('\r\n')[0])
        except KeyError:
            response = f'SIP/2.0 404 User Not Found\r\n\r\n'
            sock.sendto(response.encode('utf-8'), client_address)
            print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')
        else:
            user = self.search_address(received.split()[1])
            if user == 'User Not Found':
                response = f'SIP/2.0 404 User Not Found\r\n\r\n'
                sock.sendto(response.encode('utf-8'), client_address)
                print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')
            else:
                response = f'SIP/2.0 302 Moved Temporarily\r\nContact: {user}\r\n\r\n'
                sock.sendto(response.encode('utf-8'), client_address)
                print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')

    def handle_register(self, sock, received, client_address):
        sip = received.split()[1]
        real = f'{sip.split("@")[0]}@{client_address[0]}:{client_address[1]}'
        self.pregister(sip, real)
        response = 'SIP/2.0 200 OK\r\nFrom: <127.0.0.1:6001>\r\n\r\n'
        sock.sendto(response.encode('utf-8'), client_address)
        print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')


def main():
    port = get_arg()

    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f'{get_time()} Starting...')
        serv.serve_forever()
    except OSError as e:
        sys.exit(f"Error starting to listen: {e.args[1]}.")
    except KeyboardInterrupt:
        print("Server terminated.")
        sys.exit(0)


if __name__ == '__main__':
    main()

